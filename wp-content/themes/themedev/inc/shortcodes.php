<?php

add_shortcode( 'copyright', 'copyright_info' );
function copyright_info() {

	printf( '<p>Copyright &copy %1$d <a href="%2$s" title="%3$s">%3$s</a>, All Rights Reserved.</p>',
		date('Y'),
		esc_url( home_url( '/' ) ),
		get_bloginfo( 'name' ) );

	printf( '<small>Designed by Compulse Integrated Marketing</small>');

}

add_shortcode( 'the_facts', 'display_facts' );
function display_facts() {

	$facts = new WP_Query(array(
		'post_type' => 'facts',
		'order'     => 'DESC'
	));
	ob_start();
	if( $facts->have_posts()) { ?>
		<div class="container">
			<div class="card-columns">
				<?php while ( $facts->have_posts() ) {
					$facts->the_post(); ?>
					<div class="card card-body bg-gray">
						<div class="source text-muted pb-2">Source: <?php echo get_post_meta( get_the_ID(), 'source', true ); ?></div>
						<?php echo get_the_content(); ?>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php }
	wp_reset_postdata();
	return ob_get_clean();
}

add_shortcode( 'youtube', 'responsive_youtube_videos' );
function responsive_youtube_videos( $atts ) {

	$atts = shortcode_atts( array(
		"src" => ''
	), $atts);

	ob_start(); ?>

	<div class="video-responsive">
		<iframe width="420" height="315" src="<?php echo $atts['src']; ?>" frameborder="0" allowfullscreen></iframe>
	</div>

	<?php

	return ob_get_clean();
}