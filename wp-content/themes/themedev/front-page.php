<?php
/**
 * The template for the homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package themedev
 */

get_header();
?>
    <div class="jumbotron jumbotron-fluid vertical-align overflow-hidden position-relative">
        <video id="hg-video" autoplay loop>
            <source src="<?php echo get_template_directory_uri() . '/vids/bg_takebackbaltimore.webm'; ?>" type="video/webm">
            <source src="<?php echo get_template_directory_uri() . '/vids/bg_takebackbaltimore.mp4'; ?>" type="video/mp4">
            <?php /*  <source src="<?php echo get_template_directory_uri() . '/media/moss_dist_hp.mov'; ?>" type="video/mov"> */ ?>
            Sorry, your browser doesn't support embedded videos.
        </video>
        <div class="container text-white text-center">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo get_bloginfo( 'name' ) ; ?>"><?php compulse_display_logo(); ?></a>
            <h1 class="mt-5 mb-4">We the 600,000 citizens of Baltimore have had enough!</h1>
            <p class="lead mb-5">Show your support for a strong city. It's time to Take Back Baltimore!</p>
            <p><a class="btn btn-primary btn-lg" href="/the-community/" role="button">Take our Survey</a></p>
        </div>
    </div>
    <div id="primary" class="content-area">
	    <?php
        /*
         * TODO: Latest Videos
         */
        ?>
        <section id="latest-videos" class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center pb-4">
                        <h2>Latest Video</h2>
                    </div>
                   <div class="col-12">
                       <?php echo do_shortcode('[youtube src="http://sinclairstoryline.com/resources/embeds/jw8-embed.html?client=googima&file=https://content.uplynk.com/aa8d99fef87d455bab3948b6e18b3e42.m3u8&autostart=false"]' ); ?>
                   </div>
                    <div class="col-12 text-center my-5">
                        <a href="/the-community/" class="btn btn-primary btn-lg">Take our Survey</a>
                    </div>
                </div>
            </div>
        </section>
	    <?php
	    /*
		 * TODO: Select Your District
		 */
	    ?>
        <section id="district" class="bg-gray py-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center overflow-hidden pb-3">
                        <h2>Select Your District</h2>
                        <div id="map-wrapper" class="d-inline-block position-relative">
                            <?php echo file_get_contents(get_stylesheet_directory_uri() . '/images/map2.svg'); ?>
                            <img src="<?php echo get_template_directory_uri() . '/images/map3.jpg'; ?>" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </section>
	    <?php
	    /*
		 * TODO: Survey CTA
		 */
	    ?>
        <section id="survey-cta">
            <div class="container">
                <div class="row">
                    <div class="col-12 py-5">
                        <p class="display-5">We need to stick together as a community.</p>
                        <p class="display-5">Take our survey, then leave a comment.</p>
                        <p class="mt-4"><a href="/the-community/" class="btn btn-secondary btn-lg">Take our Survey</a></p>
                    </div>
                </div>
            </div>
        </section>
	</div><!-- #primary -->
<?php

get_footer();

