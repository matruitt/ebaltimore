<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package themedev
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <?php // needs to be moved to functions or done via gulp ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/4eba131707.js" crossorigin="anonymous"></script>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <?php
        /*
         * Custom do_action for us to tie in pixels etc that need to be tossed in after the body tag.
         */
        do_action('compulse_after_body_open');
        ?>
        <div id="page" class="site">
            <?php // Accessibility to skip to content ?>
            <a class="sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'themedev' ); ?></a>
            <nav class="navbar navbar-dark d-lg-none mobile-navbar sticky-top">
                <div class="site-branding">
			        <?php
			        //the_custom_logo(); ?>
                </div>

                <button class="navbar-toggler p-0 border-0 ml-auto" type="button" data-toggle="offcanvas">
                    <span class="navbar-toggler-icon"></span>
                    <span class="close d-none" aria-hidden="true">&times;</span>
                </button>

		        <?php
		        wp_nav_menu( array(
			        'theme_location' => 'mobile-nav',
			        'container'       => 'div',
			        'container_id'    => 'mobile-menu-wrapper',
			        'container_class' => 'navbar-collapse offcanvas-collapse',
			        'menu_class'      => 'navbar-nav mr-auto',
			        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			        'walker'          => new WP_Bootstrap_Navwalker(),
		        ) );
		        ?>
            </nav>
            <header id="masthead" class="site-header">
                <div class="container-fluid py-2">
                    <div class="row justify-content-between">
                        <nav id="site-navigation" class="main-navigation d-none d-lg-block">
		                    <?php
		                    wp_nav_menu( array(
			                    'theme_location' => 'primary-menu',
			                    'menu_id'        => 'primary-nav',
			                    'menu_class'     => 'nav',
			                    'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
			                    'walker'         => new WP_Bootstrap_Navwalker(),
		                    ) );
		                    ?>
                        </nav><!-- #site-navigation -->
                        <div class="col-md-auto">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/images/fox45-logo.png'; ?>" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>

            </header><!-- #masthead -->