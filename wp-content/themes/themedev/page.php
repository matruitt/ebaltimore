<?php
/**
 * The template for displaying all pages
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package themedev
 */

get_header();

?>
    <div class="jumbotron jumbotron-fluid vertical-align overflow-hidden" style="background-image: url( <?php echo has_post_thumbnail() ? get_the_post_thumbnail_url() : get_template_directory_uri() . '/images/interior-bg-1.jpg'; ?> );">
        <div class="container text-white text-center">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo get_bloginfo( 'name' ) ; ?>"><?php compulse_display_logo(); ?></a>
            <h1 id="sub-title" class="mt-3"><?php the_title(); ?></h1>
        </div>
    </div>
	<div id="primary" class="page-wrapper">
        <div class="container">
            <div class="row">
                <main id="main" class="site-main mb-12 <?php echo is_active_sidebar( 'sidebar-1' ) ? 'col-9' : 'col-12'; ?>">
                    <?php
                    while ( have_posts() ) :
                        the_post();

                        get_template_part( 'template-parts/content', 'page' );

                    endwhile; // End of the loop.
                    ?>
                </main><!-- #main -->
            </div> <!-- .row -->
        </div> <!-- .container -->
    </div> <!-- #primary .page-wrapper -->
<?php get_footer();
