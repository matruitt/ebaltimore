<?php


    $districts = new WP_Query(array(
	    'post_type' => 'districts',
	    'post_status' => 'publish',
	    'order'     => 'DESC',
        'posts_per_page' => -1
    ));

    If( $districts->have_posts() ) {
        while( $districts->have_posts() ) {
            $districts->the_post(); ?>
            <?php $district = get_field('district' ); ?>
            <div class="modal fade" id="<?php echo $district['value']; ?>" tabindex="-1" role="dialog" aria-labelledby="CenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>" class="rounded-circle img-fluid">
                                    </div>
                                    <div class="col-12 col-md-10">
                                        <h5 class="modal-title" id="LongTitle"><?php echo get_the_title(); ?></h5>
                                        <p class="district-number"><?php echo $district['label']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
	                        <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <h5>Councilman</h5>
	                                    <?php if ( have_rows( 'council_info' ) ) : ?>
		                                    <?php while ( have_rows( 'council_info' ) ) : the_row(); ?>
                                                <p class="m-0">Salary: <?php the_sub_field( 'salary' ); ?></p>
			                                    <p class="m-0">Elected: <?php the_sub_field( 'elected' ); ?></p>
			                                    <p class="m-0">Chair: <?php the_sub_field( 'chair' ); ?></p>
			                                    <p class="m-0"><?php the_sub_field( 'staff' ); ?></p>
		                                    <?php endwhile; ?>
	                                    <?php endif; ?>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <h5>Demographics</h5>
	                                    <?php if ( have_rows( 'demographics' ) ) : ?>
		                                    <?php while ( have_rows( 'demographics' ) ) : the_row(); ?>
			                                    <p class="m-0"><?php the_sub_field( 'residents' ); ?> Residents</p>
                                                <p class="m-0"><?php the_sub_field( 'african_american' ); ?> African American</p>
                                                <p class="m-0"><?php the_sub_field( 'white' ); ?> White</p>
                                                <p class="m-0"><?php the_sub_field( 'hispanic' ); ?> Hispanic</p>
                                                <p class="m-0"><?php the_sub_field( 'asian' ); ?> Asian</p>
		                                    <?php endwhile; ?>
	                                    <?php endif; ?>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <h5>Neighborhood</h5>
	                                    <?php if ( have_rows( 'neighborhood' ) ) : ?>
		                                    <?php while ( have_rows( 'neighborhood' ) ) : the_row(); ?>
                                                <p class="m-0">Schools: <?php the_sub_field( 'schools' ); ?></p>
		                                    <?php endwhile; ?>
	                                    <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
    }
    wp_reset_postdata();