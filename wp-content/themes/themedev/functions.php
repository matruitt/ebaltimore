<?php
/**
 * themedev functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package themedev
 */

if ( ! function_exists( 'themedev_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function themedev_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on themedev, use a find and replace
		 * to change 'themedev' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'themedev', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Setting for Gutenberg support. Uncomment is using block editor vs Classic and need it.
		 * Enabling theme support for align full and align wide option for the block editor, use
		 */
		//add_theme_support( 'align-wide' );

		/*
		* Add theme support for selective refresh for widgets.
		* Temp removal till I decide if I'm going to bother with customizer
		*/
		//add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo' );

		/*
		 *  This adds Yoast SEO plugin support for breadcrumbs to the WP customizer.
		 *  Note: Same options are under the plugin settings as well.
		 */
		add_theme_support( 'yoast-seo-breadcrumbs' );

		/*
		 * This theme uses 3 nav menus.
		 * Primary - Main navigation menu for desktops and most tablets
		 * Mobile - Menu dedicated  to just mobile/smaller tablets
		 * Quick - Footer links: May need to be expanded depending on site design
		 */
		register_nav_menus( array(
			'primary-menu'   => esc_html__( 'Primary Menu', 'themedev' ),
			'mobile-menu'    => esc_html__( 'Mobile Menu', 'themedev'),
			'quick-links'    => esc_html__( 'Quick Links', 'themedev')
		) );

		/**
		 * Register Bootstrap 4 Navigation Walker
		 */
		require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
	}
endif;
add_action( 'after_setup_theme', 'themedev_setup' );

/**
 * Add custom image sizes
 */
add_image_size( 'header-size', 2000, 768, true );
add_image_size( 'hp-logo' , 678, 274, true );
add_image_size( 'sub-logo', 554, 223, true );

/**
 * Enqueue scripts and styles.
 */
function themedev_scripts() {
    wp_deregister_script('jquery');
	// Adobe Fonts
	wp_enqueue_style('adobe-fonts', 'https://use.typekit.net/ikt3xfv.css' );
	// Theme Styles
	wp_enqueue_style( 'themedev-style', get_template_directory_uri() . '/css/style.css' );

	wp_enqueue_script( 'themedev-navigation', get_template_directory_uri() . '/js/navigation.js', array(), null, true );

	wp_enqueue_script( 'themedev-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'themedev_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


require get_template_directory() . '/inc/shortcodes.php';

/*
 * Breadcrumbs function to check if Yoast is load and if not use your own.
 * Advise using Yoast since this allows them to be admin controlled for greater flexibility
 */
function compulse_breadcrumbs() {

	// TODO: Add custom breadcrumb if Yoast is not installed. Additional option for customizer to allow display or not of breadcrumbs period.
	if ( function_exists('yoast_breadcrumb') ) {

		yoast_breadcrumb( '<nav id="breadcrumbs" class="mb-3" aria-label="breadcrumb">', '</nav>' );

	}

}

function compulse_display_logo() { ?>
	<?php if ( is_front_page() ) { ?>
        <img src="<?php echo wp_get_attachment_image_url( get_theme_mod( 'custom_logo' ), 'hp-logo' ); ?>"
             class="img-fluid">
	<?php } else { ?>
        <img src="<?php echo wp_get_attachment_image_url( get_theme_mod( 'custom_logo' ), 'sub-logo' ); ?>"
             class="img-fluid">
	<?php }
}