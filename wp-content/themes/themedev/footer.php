<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package themedev
 */

?>

        <footer id="colophon" class="site-footer">
            <div id="footer-nav" class="bg-black py-4 text-white text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-12 pt-3 pb-3">
                            <nav id="footer-nav" class="main-navigation">
                                <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'quick-links',
                                    'menu_id'        => 'quick-links',
                                    'menu_class'     => 'nav justify-content-center',
                                    'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'         => new WP_Bootstrap_Navwalker(),
                                ) );
                                ?>
                            </nav>
                        </div>
                        <div class="col-12 pb-3">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/images/footer-flag.png' ?>" alt="" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12">
                            <h3>It's Time to take back Baltimore!</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-info">
                <div class="container py-5">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/images/fox45-logo.png'; ?>" alt="Fox45 Logo" class="img-fluid d-block mx-auto mb-3">
                            &copy;2019 - All rights Reserved.
                        </div>
                        <div class="col-12  mt-3 text-center">
                            <a href="/privacy-policy/">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer><!-- #colophon -->
    </div><!-- #page -->
<?php get_template_part( 'template-parts/modal', 'districts' ); ?>
<script type="text/javascript">
    (function($) {
        $("g").click(function () {
            let district = '#' + $(this).data("target");
            $(district).modal('toggle');
        });
    })(jQuery);
</script>
    <?php wp_footer(); ?>

</body>
</html>
