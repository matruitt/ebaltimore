'use strict';

let gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    cleanCSS = require('gulp-clean-css'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    maps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create(),
    cfg = require('./gulpconfig.json');

gulp.task('minify-css', function () {
    return gulp.src('css/*.css')
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('css/'))
});

gulp.task('sass', function () {
    return gulp.src('sass/*.scss')
        .pipe(maps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(rename('style.css'))
        .pipe(postcss([autoprefixer()]))
        .pipe(maps.write('./'))
        .pipe(gulp.dest('css/'))
        .pipe(browserSync.stream()) // Inject Styles
});

gulp.task('browser-sync', function() { 
    
    browserSync.init(cfg.browserSyncWatchFiles, cfg.browserSyncOptions);
    
});

gulp.task('watch', function () {
    gulp.watch('sass/**/*.scss', ['sass']);
});

gulp.task('default', ['sass','browser-sync','watch']);